from dtaidistance import dtw
from dtaidistance import dtw_visualisation as dtwvis
import json
import numpy as np
import os
import glob
import sys
import yaml
import time
import logging
import uuid

path_in = "/test/data-in/"
path_out = "/test/data-out/"
path_query = "/shared/"


request_id = uuid.uuid4()

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

comparison_type = os.environ['comparisonType']
desired_files = "*" + comparison_type + ".list"

files = glob.glob(os.path.join(path_query, desired_files))


for filename in files:

	start = time.process_time()

	y=0
	s = []
	idd = []
	query_shot = ""

	query_shot = filename
	a = filename[len(path_query):].lstrip("/")
	(file, ext) = os.path.splitext(a)
	file_info = file.split("_")
	idd.append(int(file_info[1]))
	with open(os.path.join(path_query, filename), 'r') as f:
		s1 = np.fromfile(f, dtype=float, count=-1, sep=' ')
		s1.reshape(-1,2)
		s.append(s1)


	# read files from /mnt/data-in/

	for input_filename in glob.glob(os.path.join(path_in, desired_files)):
		a = input_filename[len(path_in):].lstrip("/")
		(file, ext) = os.path.splitext(a)
		file_info = file.split("_")
		idd.append(int(file_info[1]))
		with open(os.path.join(path_in, input_filename), 'r') as f:
			s1 = np.fromfile(f, dtype=float, count=-1, sep=' ')
			s1.reshape(-1,2)
			s.append(s1)

	pos = 0

	for i in range (len(s)):
		if y != pos:
			start_dtw_time = time.process_time()
			distance, paths = dtw.warping_paths(s[pos], s[y])
			elapsed_time = time.process_time() - start_dtw_time

			with open(f'{path_out}/{idd[0]}_{idd[y]}.distance', 'w') as file:
				file.write(str(distance))
				file.close
			with open(f'{path_out}/{idd[0]}_{idd[y]}.time', 'w') as file:
				file.write(str(elapsed_time))
				file.close
			logging.info("request_id:" + str(request_id) + " " + "query_shot_id:" + str(idd[0]) + " " + "shot_id:" + str(idd[y]) + " " + "total_time:" + str(elapsed_time) + " " + "distance:" + str(distance))

		y = i+1


	total_time = time.process_time() - start

	with open(f'{path_out}/{idd[0]}.total_time', 'w') as file:
				file.write(str(total_time))
				logging.info("request_id:" + str(request_id) + " " + "query_shot_id:" + str(idd[0]) + " " + "total_time:" + str(total_time))
				file.close





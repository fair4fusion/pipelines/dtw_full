FROM python:3.9-slim

RUN apt update && apt install -y git \
	gcc \
	make

RUN apt-get install python3-pip -y

COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

# RUN mkdir data/
COPY dtw.py dtw.py
# COPY /data/* data/

# EXPOSE 8000
# VOLUME /var/lib/data
# RUN mkdir output/

ENTRYPOINT ["python3","dtw.py"]
